﻿using Microsoft.Extensions.Configuration;
using Microsoft.FSharp.Linq.RuntimeHelpers;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection.Metadata;
using System.IO;



namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            // Создание файла журнала доступа
            CreateLogFile();

            // Чтение параметров из файлов конфигурации и переменных среды
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .AddJsonFile(@"D:\project\Test\Test\appsettings.json", optional: true) // Параметр optional указывает, что файл необязательный
                .AddEnvironmentVariables()
                .Build();

            // Инициализация переменных с путями к файлам
            string logFilePath = configuration["logFilePath"];
            string outputFilePath = configuration["outputFilePath"];
            Console.WriteLine($"Log file path: {logFilePath}");
            Console.WriteLine($"Output file path: {outputFilePath}");

            // Проверка наличия файлов
            if (!File.Exists(logFilePath))
            {
                Console.WriteLine("Log file not found.");
                return;
            }

            // Инициализация остальных переменных
            string addressStart = configuration["addressStart"];
            string addressMask = configuration["addressMask"];
            DateTime startTime = DateTime.ParseExact(configuration["startTime"], "dd.MM.yyyy", null);
            DateTime endTime = DateTime.ParseExact(configuration["endTime"], "dd.MM.yyyy", null);

            // Если переданы аргументы командной строки, они имеют приоритет перед параметрами из файла конфигурации
            ParseCommandLineArguments(args, ref logFilePath, ref outputFilePath, ref addressStart, ref addressMask, ref startTime, ref endTime);

            // Чтение строк из файла журнала
            IEnumerable<string> logLines;
            try
            {
                logLines = File.ReadLines(logFilePath);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error reading log file: {ex.Message}");
                return;
            }

            // Фильтрация и группировка IP-адресов
            var filteredLogs = FilterAndGroupIPs(logLines, startTime, endTime, addressStart, addressMask).ToList();
            foreach (var log in filteredLogs)
            {
                Console.WriteLine(log);
            }
            // Запись результатов в файл
            try
            {
                File.WriteAllLines(outputFilePath, filteredLogs);
                Console.WriteLine($"Analysis complete. Results written to file: test.txt");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error writing output file: {ex.Message}");
            }
        }

        static void CreateLogFile()
        {
            // Путь к файлу журнала доступа
            string filePath = "log_file.log";

            // Пример данных журнала доступа (адрес и время запроса)
            string[] logData = new string[]
            {
            "192.168.1.1 2024-04-05 12:30:45",
            "172.168.1.2 2024-04-05 13:15:22",
            "192.168.1.3 2024-04-05 14:20:10"
            };

            // Запись данных в файл
            try
            {
                using (StreamWriter writer = new StreamWriter(filePath))
                {
                    foreach (string logEntry in logData)
                    {
                        writer.WriteLine(logEntry);
                    }
                }

                Console.WriteLine($"Log file created successfully: {filePath}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error creating log file: {ex.Message}");
            }
        }

        static IEnumerable<string> FilterAndGroupIPs(IEnumerable<string> logLines, DateTime startTime, DateTime endTime, string addressStart, string addressMask)
        {
            return logLines
                .Select(line => line.Split(' '))
                .Where(parts => parts.Length == 3) // Проверка, что строка разбита на IP, дату и время
                .Select(parts =>
                {
                    string ipAddress = parts[0];
                    DateTime timeStamp;
                    if (DateTime.TryParseExact(parts[1] + " " + parts[2], "yyyy-MM-dd HH:mm:ss", null, DateTimeStyles.None, out timeStamp))
                    {
                        return new { IP = ipAddress, TimeStamp = timeStamp };
                    }
                    return null; // Если не удалось преобразовать временную метку, возвращаем null
                })
                .Where(entry => entry != null && entry.TimeStamp >= startTime && entry.TimeStamp <= endTime && IsAddressInRange(entry.IP, addressStart, addressMask))
                .GroupBy(entry => entry.IP)
                .Select(group => $"{group.Key}: {group.Count()} requests");
        }

        static bool IsAddressInRange(string ipAddress, string addressStart, string addressMask)
        {
            if (string.IsNullOrEmpty(addressStart))
                return true;

            if (string.IsNullOrEmpty(addressMask))
                return ipAddress.StartsWith(addressStart);

            string[] maskParts = addressMask.Split('.');
            string[] ipParts = ipAddress.Split('.');

            for (int i = 0; i < maskParts.Length; i++)
            {
                int mask = int.Parse(maskParts[i]);
                int ip = int.Parse(ipParts[i]);

                if ((ip & mask) != (int.Parse(addressStart.Split('.')[i]) & mask))
                    return false;
            }

            return true;
        }

        static void ParseCommandLineArguments(string[] args, ref string logFilePath, ref string outputFilePath, ref string addressStart, ref string addressMask, ref DateTime startTime, ref DateTime endTime)
        {
            for (int i = 0; i < args.Length; i += 2)
            {
                switch (args[i])
                {
                    case "--file-log":
                        logFilePath = args[i + 1];
                        break;
                    case "--file-output":
                        outputFilePath = args[i + 1];
                        break;
                    case "--address-start":
                        addressStart = args[i + 1];
                        break;
                    case "--address-mask":
                        addressMask = args[i + 1];
                        break;
                    case "--time-start":
                        startTime = DateTime.ParseExact(args[i + 1], "dd.MM.yyyy", null);
                        break;
                    case "--time-end":
                        endTime = DateTime.ParseExact(args[i + 1], "dd.MM.yyyy", null);
                        break;
                    default:
                        Console.WriteLine($"Unknown argument: {args[i]}");
                        break;
                }
            }
        }
    }
}

